
#define MIDI_SHIELD_LED_D6 PH3
#define MIDI_SHIELD_LED_D6_PORT PORTH
#define MIDI_SHIELD_LED_D6_DDR  DDRH

#define DEBOUNCE_PERIOD 20
#define BLINK_LED_PERIOD 999
#define MIDI_CHANNEL 1
#define RAW_THRESHOLD 20

// Create and bind the MIDI interface to the default hardware Serial port

typedef enum
{
  initState,
  beginRunState,
} stateMachine_t ;


void setup() {
  // Set D6(PH3) as OUTPUT
  MIDI_SHIELD_LED_D6_DDR |= (1 << MIDI_SHIELD_LED_D6 ) ;

  Serial.begin(31250) ;

  MIDI_TX(0x90, 36 /*PadNote[pin]*/, 127) ;
}

void loop() {

  static unsigned long ulCurrentTime = 0 ;

  static unsigned long ulNextDebounceTime = 0 ;
  static unsigned long ulNextTimeLED = 0 ;

  static byte an0_raw_read = 0 ;
  static byte an0_raw_peak = 0 ;

  ulCurrentTime = millis() ;

  an0_raw_read = analogRead(A0) ;

  if ( an0_raw_read > (an0_raw_peak + RAW_THRESHOLD) )
  {
    an0_raw_peak = an0_raw_read ;
    ulNextDebounceTime = ulCurrentTime + DEBOUNCE_PERIOD ;
  }

  if ( ulCurrentTime > ulNextDebounceTime )
  {
    
    MIDI_TX(0x90, 38 /*PadNote[pin]*/, 127);
  }

  if ( ulCurrentTime > ulNextTimeLED )
  {
    //MIDI_TX(0x90, 36 /*PadNote[pin]*/, 127);
    MIDI_SHIELD_LED_D6_PORT ^= (1 << MIDI_SHIELD_LED_D6 ) ;
    ulNextTimeLED = ulCurrentTime + BLINK_LED_PERIOD ;
  }

}

//*******************************************************************************************************************
// Transmit MIDI Message
//*******************************************************************************************************************
void MIDI_TX(byte MESSAGE, byte PITCH, byte VELOCITY)
{
  byte status1 = MESSAGE + (midichannel - 1);
  Serial.write(status1);
  Serial.write(PITCH);
  Serial.write(VELOCITY);

}


/*void playMIDINote(byte channel, byte note, byte velocity)
  {
    //MIDI channels 1-16 are really 0-15
    byte noteOnStatus = 0x90 + (channel-1);

    //Transmit a Note-On message
    Serial.write(noteOnStatus);
    Serial.write(note);
    Serial.write(velocity);
  } */
